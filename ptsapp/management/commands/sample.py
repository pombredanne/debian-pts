#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from ptsapp.sample_model import make_model

class Command(BaseCommand):
    help = 'Make a sample model'

    def handle(self, *args, **kwargs):
        make_model(name=args[0])
