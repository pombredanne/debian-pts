#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone

# For each email+package subscription, you have a certain set of keywords,
# some of which are default.

class Package(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __unicode__(self):
        return self.name

class Subscription(models.Model):
    """A subscription is an email+package, which can have multiple tags/keywords"""
    address = models.EmailField(max_length=254)
    package = models.ForeignKey(Package)
    tags = models.ManyToManyField('Tag')
    created_on = models.DateTimeField(default=timezone.now())

    # Since Django doesn't come with a multiple-choice model field type,
    # for now, we shall check for field names here manually against a list.
    DEFAULT_TAGS = ['default', 'bts', 'bts-control', 'summary', 'upload-source',
                    'katie-other', 'contact', 'buildd']

    AVAILABLE_TAGS = DEFAULT_TAGS + ['cvs', 'ddtp', 'upload-binary',
                            'derivatives', 'derivatives-bugs']

    # Convenience methods for keywords associated with /this/ subscription
    def get_tags(self):
        return self.tags.all()

    def set_tag(self, tag_name):
        if tag_name in self.AVAILABLE_TAGS:
            t = Tag.objects.get_or_create(name=tag_name)[0]
            self.tags.add(t)
        else:
            raise ValueError("Invalid Tag name")

    def delete_tag(self, tag_name):
        t = Tag.objects.get(name=tag_name)
        self.tags.remove(t)

    def generate_default_tags(self):
        """Default tags that are associated with every new subscription"""
        for tag_name in self.DEFAULT_TAGS:
            t = Tag.objects.get_or_create(name=tag_name)[0]
            self.tags.add(t)

    def __unicode__(self):
        return "%s:%s" % (self.address, self.package)

class Tag(models.Model):
    """A subscription can have many tags"""
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class Bounce(models.Model):
    """List of bounced emails is kept in a separate table"""
    date  = models.DateTimeField(default=timezone.now())
    email = models.EmailField()
    sent  = models.BooleanField()              # True: Sent, False: Bounced

    def __unicode__(self):
        return "%s[%s]" % (self.email, self.sent)
