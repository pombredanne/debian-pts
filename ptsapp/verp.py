#!/usr/bin/env python
# -*- coding: utf-8 -*-

# VERP Encoding in Python, a partial rewrite of CPAN's Mail::Verp.

import re

CHARS = '@|:|%|!|-|[|]'.split('|')

def encode_chars(char):
    return re.escape(char), "%.2X" % ord(char)

def decode_chars(char):
    return "%.2X" % ord(char), char

ENCODE_MAP = dict(map(encode_chars, ['+'] + CHARS))
DECODE_MAP = dict(map(decode_chars, CHARS + ['+']))
SEPERATOR = '-'

def encode(sender, recipient):
    """Encode the recipient email address"""
    smatch = re.search(r'(.+)\@([^\@]+)$', sender)
    if smatch:
        slocal = smatch.group(1)
        sdomain = smatch.group(2)
    else:
        print("Can't parse sender address")
        return

    rmatch = re.search(r'(.+)\@([^\@]+)$', recipient)
    if rmatch:
        rlocal = rmatch.group(1)
        rdomain = rmatch.group(2)
    else:
        print("Can't parse recipient address")
        return

    # Substitute all occurances of key with val for (key, val) in ENCODE_MAP
    # re.sub on '|'.join(map(re.escape, yourdict)) with the function callback of d.get
    # Ref: http://docs.python.org/2/library/re.html#re.sub
    rlocal = re.sub('|'.join(ENCODE_MAP), ENCODE_MAP.get, rlocal)
    rdomain = re.sub('|'.join(ENCODE_MAP), ENCODE_MAP.get, rdomain)
    return ''.join([slocal, SEPERATOR, rlocal, '=', rdomain, '@', sdomain])


if __name__ == '__main__':
    print(encode("jack@foo.com", "jill@bar.com"))
