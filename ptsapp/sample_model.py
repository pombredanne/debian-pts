#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sample models for quick testing
from models import Subscription, Tag, Package

def make_model(name='python'):
    pkg = Package.objects.get_or_create(name=name)
    package = pkg[0]
    package.save()

    sub = Subscription(address='hello@example.com', package=package)
    sub.save()
    sub.generate_default_tags()
    sub.save()

